(ns mpn.cd36415)

;; on the parker repl:
;; (load-file "/Users/leonardo.nobrega/Documents/code/mpn/src/clj/mpn/cd36415/parker.clj")

;; does a 9k have sub-interfaces with v6 ips?
(defn ne-ipv6-interfaces
  [hostname]
  (datomic.api/q
   '[:find ?if
     :in $ ?hostname
     :where
     [?ne :network-element/hostname ?hostname]
     [?ip :ip-address/type "ipv6"]
     [?ne :network-element/interface ?if]
     [?if :interface/sub-interface ?sub]
     [?sub :sub-interface/ip-address ?ip]]
   (cenx.rostra.document-repository/database-for-read)
   hostname))

;; are there ip-addresses with type ipv6 and no address?
(defn count-nil-v6-ips
  []
  (datomic.api/q
   '[:find (count ?ip)
     :in $
     :where
     [?ip :ip-address/type "ipv6"]
     [(missing? $ ?ip :ip-address/address)]]
   (cenx.rostra.document-repository/database-for-read)))

;; names of nes with vrfs that contain nil v6 ips
(defn nes-with-vfrs-with-nil-v6-ips
  []
  (datomic.api/q
   '[:find ?hostname
     :in $
     :where
     [?ne :network-element/virtual-router ?vr]
     [?ne :network-element/hostname ?hostname]
     [?vr :virtual-router/sub-interface ?s]
     [?s :sub-interface/ip-address ?ip]
     [?ip :ip-address/type "ipv6"]
     [(missing? $ ?ip :ip-address/address)]]
   (cenx.rostra.document-repository/database-for-read)))

;; find service name using hostname prefix
(defn hostname->mpn-name
  [re-name]
  (datomic.api/q
   '[:find ?vr-id
     :in $ ?re-name
     :where
     [?ne :network-element/hostname ?hn]
     [(re-find ?re-name ?hn)]
     [?ne :network-element/virtual-router ?vr]
     [?vr :virtual-router/id ?vr-id]
     [(.contains ?vr-id "MPN")]]
   (cenx.rostra.document-repository/database-for-read)
   re-name))
