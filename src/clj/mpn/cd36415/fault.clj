(ns mpn.cd36415.fault
  (:require [clojure.set :as set]
            [cenx.heimdallr.debug :as dbg]
            [cenx.heimdallr.customers.shared.vzw.debug :as vzw-dbg]
            [cenx.heimdallr.fault-graph.graph :as gr]))

;; (load-file "/Users/leonardo.nobrega/Documents/code/mpn/src/clj/mpn/cd36415/fault.clj")

(def snc-id "aggregate:SNC:ELSSTX135AE:MPN02680")

(defmacro defn-pp
  [name args body]
  `(do (defn ~name ~args ~body)
       (defn ~(-> name (str "-pp") symbol)
         ~args
         (let [f# (comp clojure.pprint/pprint ~name)]
           (apply f# ~args)))))

(defn-pp children
  [id]
  (->> id
       dbg/get-object
       :fault/depends_on
       (map :elm-entity/key)
       sort))

(defn state
  [id]
  (-> id dbg/get-object :fault/state))

(defn-pp children-states
  [id]
  (let [nodes (children id)
        states (map state nodes)]
    (apply sorted-map (interleave nodes states))))

(defn find-descendant-alarms
  [id]
  (let [children (->> id dbg/get-object :fault/depends_on)
        alarm? #(= (:fault/type %) :alarm)
        my-filter (fn [f-r children]
                    (let [f (if f-r filter remove)]
                      (->> children (f alarm?) (map :elm-entity/key))))
        alarm-children (my-filter true children)
        not-alarm-children (my-filter false children)]
    (set (concat alarm-children
                 (mapcat find-descendant-alarms not-alarm-children)))))

(defn one-descendant-alarm
  [id]
  (-> id find-descendant-alarms first))

(defn common-alarm
  ([[id1 id2]]
   (common-alarm id1 id2))
  ([id1 id2]
   (set/intersection (find-descendant-alarms id1)
                     (find-descendant-alarms id2))))

(defn make-pairs
  [elements]
  (let [pairs-with-1st (fn [lst]
                         (let [f (first lst)
                               r (rest lst)]
                           (map #(vector f %) r)))]
    (loop [pairs '()
           lst elements]
      (if (empty? lst)
        pairs
        (recur (concat pairs
                       (pairs-with-1st lst))
               (rest lst))))))

(defn children-with-common-descendant-alarm
  [id]
  (let [children (children id)
        alarms find-descendant-alarms
        child-alarms (zipmap children (map alarms children))
        pairs (make-pairs children)
        have-common? #(not (empty? (common-alarm %1 %2)))]
    (filter #(apply have-common? %) pairs)))

(defn unique-alarms-of-pair-elements
  ([[id1 id2]]
   (unique-alarms-of-pair-elements id1 id2))
  ([id1 id2]
   (let [alarms1 (find-descendant-alarms id1)
         alarms2 (find-descendant-alarms id2)]
     {id1 (set/difference alarms1 alarms2)
      id2 (set/difference alarms2 alarms1)})))

(defn fail-alarms
  [ids]
  (->> ids (map #(vzw-dbg/send-vsm % :Failed)) dorun))

(defn fault-data->str
  [fault-data]
  (->> fault-data
       (map (juxt :sim/label :sim/value))
       flatten
       (apply hash-map)
       clojure.pprint/pprint
       with-out-str))

(def ^:dynamic remove-pred (constantly false))

(defn obj-name->graph
  [node-name]
  (let [remove-node-fn #(-> % second (get "public_id") remove-pred)
        update-nodes (fn [g] (update-in g [:nodes]
                                        #(remove remove-node-fn %)))

        node-in-edge (fn [node attr] (-> node second (get attr)))
        remove-edge-fn #(or (remove-pred (node-in-edge % "src"))
                            (remove-pred (node-in-edge % "target")))
        update-edges (fn [g] (update-in g [:edges]
                                        #(remove remove-edge-fn %)))]
    (->> node-name
         dbg/get-object
         (gr/datomic-graph (gr/empty-graph))
         update-nodes
         update-edges)))

(defn graph-nodes
  [node-name]
  (-> node-name obj-name->graph :nodes keys))

(defn make-is-descendant?
  [ancestor]
  (let [descendants (-> ancestor graph-nodes set)]
    (fn [node-name] (-> node-name descendants boolean))))

(defn any-predicates-true?
  [preds]
  (fn [value]
    (->> preds
        (map #(% value))
        (reduce #(or %1 %2) false))))

(defn make-descendant-of-any?
  [node-names]
  (->> node-names (map make-is-descendant?) any-predicates-true?))

(defn gml
  [node-name file]
  (-> node-name
      obj-name->graph
      (gr/to-gml-file file)))

(defn gml-without-descendants
  [node-name nodes-to-remove file]
  (with-bindings {#'remove-pred (make-descendant-of-any? nodes-to-remove)}
    (gml node-name file)))
