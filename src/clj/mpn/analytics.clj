(ns mpn.analytics
  (:require [taoensso.timbre :as log]))

;; (load-file "/Users/leonardo.nobrega/Documents/code/mpn/src/clj/mpn/analytics.clj")

(def send-connection-issue-fn
  (fn [e & args] (log/error e args)))

(defn my-ingest
  [filename]
  (cenx.levski.utilization-topology/verizon-ingest
   "agg"                                    ;; mode
   (cenx.plataea.cassandra.session/session) ;; session
   filename
   nil                                      ;; reader-key
   :sevone                                  ;; traffic-type
   send-connection-issue-fn))
