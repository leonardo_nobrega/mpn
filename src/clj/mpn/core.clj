(ns mpn.core)

;; on the parker repl:
;; (load-file "/Users/leonardo.nobrega/Documents/code/mpn/src/clj/mpn/core.clj")

(def hpna-path "/Users/leonardo.nobrega/Downloads/hpna_archive/")
;;(def hpna-path "/Users/leonardo.nobrega/Downloads/CENX_20170110/CiscoNCM/")

(defn list-configs
  [site-name]
  (->> hpna-path
       java.io.File.
       .list
       (filter #(.startsWith % site-name))))

(defn nes
  [site-name op]
  (let [query #(vector % (cenx.rostra.document-repository/documents-with-filename %))
        remove #(map cenx.rostra.document-repository/retract-document!
                     (cenx.rostra.document-repository/documents-with-filename %))
        import #(cenx.rostra.document-repository/import-document!
                 (str hpna-path %)
                 :router-configuration
                 :global :mpn)
        dispatch #(case op
                    (:l :list-files) %
                    (:q :query)      (query %)
                    (:r :remove)     (remove %)
                    (:i :import)     (import %))]
    (map dispatch
         (list-configs site-name))))

(defmacro log->
  ;; Example usage:
  ;; (-> cp
  ;;     first
  ;;     eml2/layer-service-terminations
  ;;     (log-> log/info "HEY lsts")
  ;;     first)
  [res logger msg]
  (list 'do (list logger msg res)
        res))

(defmacro log->>
  [logger msg res]
  (list 'do (list logger msg res)
        res))

;; use to disable logs in heimdallr and parker repls
(defn disable-log
  []
  (taoensso.timbre/merge-config! {:appenders {:println {:enabled? false}}}))

;; use to format bifrost data copied from chrome dev-tools
(defn pprint-data
  [dir in-file out-file parse-fn]
  (->> (str dir "/" in-file)
       slurp
       parse-fn
       clojure.pprint/pprint
       with-out-str
       (spit (str dir "/" out-file))))

(defn pprint-transit-data
  [dir in-file out-file]
  (pprint-data dir in-file out-file
               cenx.pack.core/parse-transit))

(defn pprint-edn-data
  [dir in-file out-file]
  (pprint-data dir in-file out-file
               read-string))

(defn make-fault-test-eml
  [service-name]
  (->> service-name
       cenx.parker.strategies.vzw.v2.mpn/->eml
       clojure.pprint/pprint
       with-out-str
       (spit (str service-name ".eml"))))
