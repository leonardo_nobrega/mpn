#!/bin/sh

DEV=$1

collect () {
    cat $1 | sed "s/\(_[0-9]\+\)\+_CENX_[A-Za-z_]\+.csv.gz//" | sort | uniq
}

look () {
    # redirect to stderr
    # because otherwise bash will not print the message immediately
    # it will wait until all looks are done
    # because the output of for is piped into collect
    echo "looking for $DEV in $1* ..." >&2
    zgrep -l $DEV "$1"*
}

for i in `ls -1 | collect`; do look "$i"; done | collect
