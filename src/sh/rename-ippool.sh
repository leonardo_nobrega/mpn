#!/bin/sh

for i in `ls -1 *CENX*`; do
    NEW_NAME=`echo $i | sed -E "s/([^.]+)\.csv\.gz/\1_ip_pool.csv.gz/"`;
    echo $i "-->" $NEW_NAME;
    mv $i $NEW_NAME
done
