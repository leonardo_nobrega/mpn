#!/bin/sh

# create sqlite DB
sqlite3 ippool.db "create table ip_pool_data (device_id integer, device_name text, device_ip text, object_name text, indicator_name text, indicator_type text, timestamp text, value real, delta real, epoch integer, time_delta integer);"

# create a fifo
mkfifo tempfifo

# import into DB
# "tail -n +2" removes the header
for i in `ls -1 *.csv.gz`; do
    echo $i
    gzcat "$i" | tail -n +2 > tempfifo &
    sqlite3 ippool.db '.import tempfifo ip_pool_data'
done

# remove fifo
rm tempfifo
