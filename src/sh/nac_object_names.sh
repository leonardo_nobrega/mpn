#!/bin/sh

FILE=$1

gzcat $FILE |
    cut -d "|" -f 2,4 | # select device_name (2nd) and object_name (4th)
    grep "5[AB]E" |     # filter nacs
    cut -d "|" -f 2 |   # select the object_name
    sort |
    uniq
