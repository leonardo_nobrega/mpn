#!/bin/sh

KEYSPACE=$1

tables () {
    cqlsh -e "describe keyspace $KEYSPACE;" |
        grep "CREATE TABLE" |
        cut -d " " -f 3
}

count () {
    TABLE=$1
    cqlsh -e "use $KEYSPACE; select count(*) from $TABLE;" |
        head -n 4 | tail -n 1 |  # isolate the line with the count
        gsed "s/[ ]\+//"         # remove spaces
}

for t in `tables`; do echo $t `count $t`; done;
